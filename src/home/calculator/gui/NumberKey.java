/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package home.calculator.gui;

import javax.swing.JButton;

/**
 *
 * @author ryoh
 */
public class NumberKey extends JButton {

    private int number;

    public NumberKey() {
        super();
        this.number = 0;
    }

    public int getNumber() {
        return number;
    }

    // Because NetBeans doesn't allow editing
    // automatically generated variables
    // I set the number here
    @Override
    public void setText(String text) {
        super.setText(text);
        number = Integer.parseInt(text);
    }
}