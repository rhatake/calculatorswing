/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package home.calculator.gui;

import java.awt.event.ActionListener;

/**
 *
 * @author ryoh
 */
public interface CalculatorViewGUI {
    public void setDisplayValue(double value);
    public double getDisplayValue();
    public void overWriteDisplay();

    public void addAdditionActionListener(final ActionListener action);
    public void addSubtractionActionListener(final ActionListener action);
    public void addMultiplicationListener(final ActionListener action);
    public void addDivisionListener(final ActionListener action);
    public void addPowerListener(final ActionListener action);
    public void addSqrtListener(final ActionListener action);
    public void addCalculateListener(final ActionListener action);
    public void addClearListener(final ActionListener action);
}
