/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package home.calculator.controller;

/**
 *
 * @author ryoh
 */
public class NumberEnteredCalculatorState extends CalculatorState {

    @Override
    public double add(double amount) {
        double answer = 0;
        
        if (lastAction != Actions.ADD) {
            answer =  super.calc(amount);
        } else {
            answer =  calculator.add(amount);
        }
        
        lastAction = Actions.ADD;
        return answer;
    }

    @Override
    public double sub(double amount) {
        double answer = 0;
        
        if (lastAction != Actions.SUB) {
            answer =  super.calc(amount);
        } else {
            answer =  calculator.subtract(amount);
        }
        
        lastAction = Actions.SUB;
        return answer;
    }

    @Override
    public double mul(double amount) {
        double answer = 0;
        
        if (lastAction != Actions.MUL) {
            answer =  super.calc(amount);
        } else {
            answer =  calculator.multiply(amount);
        }
        
        lastAction = Actions.MUL;
        return answer;
    }

    @Override
    public double div(double amount) {
        double answer = 0;
        
        if (lastAction != Actions.DIV) {
            answer =  super.calc(amount);
        } else {
            answer =  calculator.divide(amount);
        }
        
        lastAction = Actions.DIV;
        return answer;
    }

    @Override
    public double pow(double amount) {
        double answer = 0;
        
        if (lastAction != Actions.POW) {
            answer =  super.calc(amount);
        } else {
            answer =  calculator.power(amount);
        }
        
        lastAction = Actions.POW;
        return answer;
    }

    @Override
    public double sqrt(double amount) {
        return calculator.squareRoot();
    }

}
