/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package home.calculator.controller;

import home.calculator.gui.CalculatorViewGUI;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author ryoh
 */
public class CalculatorController {

    private CalculatorState calculatorCurrentState;
    private final CalculatorState calculatorIdleState;
    private final CalculatorState calculatorNumberEnteredState;
    private final CalculatorState calculatorDoNothingState;
    
    private final CalculatorViewGUI guiWindow;

    //<editor-fold defaultstate="collapsed" desc=" Actions Listeners ">
    private final ActionListener addAction = new ActionListener() {

        @Override
        public void actionPerformed(ActionEvent e) {
            add();
        }

    };

    private final ActionListener subAction = new ActionListener() {

        @Override
        public void actionPerformed(ActionEvent e) {
            sub();
        }

    };

    private final ActionListener mulAction = new ActionListener() {

        @Override
        public void actionPerformed(ActionEvent e) {
            mul();
        }

    };

    private final ActionListener divAction = new ActionListener() {

        @Override
        public void actionPerformed(ActionEvent e) {
            div();
        }

    };

    private final ActionListener powAction = new ActionListener() {

        @Override
        public void actionPerformed(ActionEvent e) {
            pow();
        }

    };

    private final ActionListener sqrtAction = new ActionListener() {

        @Override
        public void actionPerformed(ActionEvent e) {
            sqrt();
        }

    };

    private final ActionListener clearAction = new ActionListener() {

        @Override
        public void actionPerformed(ActionEvent e) {
            clear();
        }

    };

    private final ActionListener calculateAction = new ActionListener() {

        @Override
        public void actionPerformed(ActionEvent e) {
            calculate();
        }

    };//</editor-fold>

    public CalculatorController(CalculatorViewGUI guiWindow) {
        this.calculatorIdleState = new IdleCalculatorState();
        this.calculatorNumberEnteredState = new NumberEnteredCalculatorState();
        this.calculatorDoNothingState = new DoNothingCalculatorState();
        this.calculatorCurrentState = this.calculatorIdleState;
        
        this.guiWindow = guiWindow;

        this.guiWindow.addAdditionActionListener(addAction);
        this.guiWindow.addSubtractionActionListener(subAction);
        this.guiWindow.addMultiplicationListener(mulAction);
        this.guiWindow.addDivisionListener(divAction);
        this.guiWindow.addPowerListener(powAction);
        this.guiWindow.addClearListener(clearAction);
        this.guiWindow.addSqrtListener(sqrtAction);
        this.guiWindow.addCalculateListener(calculateAction);
    }

    private void add() {
        calculatorCurrentState.add(guiWindow.getDisplayValue());
        guiWindow.overWriteDisplay();
        calculatorCurrentState = calculatorNumberEnteredState;
    }

    private void sub() {
        calculatorCurrentState.sub(guiWindow.getDisplayValue());
        guiWindow.overWriteDisplay();
        calculatorCurrentState = calculatorNumberEnteredState;
    }

    private void mul() {
        calculatorCurrentState.mul(guiWindow.getDisplayValue());
        guiWindow.overWriteDisplay();
        calculatorCurrentState = calculatorNumberEnteredState;
    }

    private void div() {
        calculatorCurrentState.div(guiWindow.getDisplayValue());
        guiWindow.overWriteDisplay();
        calculatorCurrentState = calculatorNumberEnteredState;
    }

    private void pow() {
        calculatorCurrentState.pow(guiWindow.getDisplayValue());
        guiWindow.overWriteDisplay();
        calculatorCurrentState = calculatorNumberEnteredState;
    }

    private void sqrt() {
        calculatorCurrentState.sqrt(guiWindow.getDisplayValue());
        guiWindow.setDisplayValue(calculatorCurrentState.getCurrentValue());
        calculatorCurrentState = calculatorDoNothingState;
    }

    private void clear() {
        calculatorCurrentState.clear();
        guiWindow.setDisplayValue(0);
        guiWindow.overWriteDisplay();
        
        calculatorCurrentState = calculatorIdleState;
    }

    private void calculate() {
        calculatorCurrentState.calc(guiWindow.getDisplayValue());
        guiWindow.setDisplayValue(calculatorCurrentState.getCurrentValue());
        
        guiWindow.overWriteDisplay();
        calculatorCurrentState = calculatorDoNothingState;
    }
}
