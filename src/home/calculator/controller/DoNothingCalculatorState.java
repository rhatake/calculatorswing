/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package home.calculator.controller;

/**
 *
 * @author ryoh
 */
public class DoNothingCalculatorState extends CalculatorState {

    @Override
    public double sqrt(double amount) {
        return calculator.squareRoot();
    }

    @Override
    public double add(double amount) {
        lastAction = Actions.ADD;
        return calculator.getCurrentValue();
    }

    @Override
    public double sub(double amount) {
        lastAction = Actions.SUB;
        return calculator.getCurrentValue();
    }

    @Override
    public double mul(double amount) {
        lastAction = Actions.MUL;
        return calculator.getCurrentValue();
    }

    @Override
    public double div(double amount) {
        lastAction = Actions.DIV;
        return calculator.getCurrentValue();
    }

    @Override
    public double pow(double amount) {
        lastAction = Actions.POW;
        return calculator.getCurrentValue();
    }
    
}
