/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package home.calculator.controller;

import home.calculator.model.CalculatorModel;

/**
 *
 * @author ryoh
 */
public abstract class CalculatorState
{
    protected static Actions lastAction = Actions.NOTHING;
    protected static CalculatorModel calculator = new CalculatorModel();
    
    protected enum Actions {
        ADD,
        SUB,
        MUL,
        DIV,
        POW,
        
        NOTHING,
    };

    public abstract double sqrt(double amount);
    public abstract double add(double amount);
    public abstract double sub(double amount);
    public abstract double mul(double amount);
    public abstract double div(double amount);
    public abstract double pow(double amount);
    
    public final void clear()
    {
        calculator.clear();
    }
    
    public final double getCurrentValue()
    {
        return calculator.getCurrentValue();
    }
    
    public final double calc(double amount)
    {
        switch (lastAction) {
            case ADD:
                add(amount);
                break;
            case SUB:
                sub(amount);
                break;
            case MUL:
                mul(amount);
                break;
            case DIV:
                div(amount);
                break;
            case POW:
                pow(amount);
                break;
        }
        
        return calculator.getCurrentValue();
    }
}
