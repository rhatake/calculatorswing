/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package home.calculator.controller;


/**
 *
 * @author ryoh
 */
public class IdleCalculatorState extends CalculatorState {   
    
    @Override
    public double add(double amount) {
        lastAction = Actions.ADD;
        return calculator.add(amount);
    }

    @Override
    public double sub(double amount) {
        lastAction = Actions.SUB;
        return calculator.add(amount);
    }

    @Override
    public double mul(double amount) {
        lastAction = Actions.MUL;
        return calculator.add(amount);
    }

    @Override
    public double div(double amount) {
        lastAction = Actions.DIV;
        return calculator.add(amount);
    }

    @Override
    public double pow(double amount) {
        lastAction = Actions.POW;
        return calculator.add(amount);
    }

    @Override
    public double sqrt(double amount) {
        calculator.add(amount);
        return calculator.squareRoot();
    }
    
}
