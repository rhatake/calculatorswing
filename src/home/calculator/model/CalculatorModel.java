/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package home.calculator.model;

/**
 *
 * @author ryoh
 */
public class CalculatorModel {

    private double currentValue = 0;

    public CalculatorModel() {

    }

    public double add(double arg) {
        currentValue += arg;
        return currentValue;
    }

    public double subtract(double arg) {
        currentValue -= arg;
        return currentValue;
    }

    public double multiply(double arg) {
        currentValue *= arg;
        return currentValue;
    }

    public double divide(double arg) // throws divide by zero
    {
        currentValue /= arg;
        return currentValue;
    }

    public double squareRoot() {
        currentValue = Math.sqrt(currentValue);
        return currentValue;
    }

    public double power(double arg) {
        currentValue = Math.pow(currentValue, arg);
        return currentValue;
    }

    public void clear() {
        currentValue = 0;
    }

    public double getCurrentValue() {
        return currentValue;
    }

}
