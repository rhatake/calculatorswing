/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package home.calculator;

import home.calculator.gui.CalculatorView;
import home.calculator.controller.CalculatorController;

/**
 *
 * @author ryoh
 */
public class Calculator {

    public static void main(String[] arg) {

        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                CalculatorView gui = new CalculatorView();
                CalculatorController calculator = new CalculatorController(gui);

                gui.setVisible(true);
            }
        });
    }
}
